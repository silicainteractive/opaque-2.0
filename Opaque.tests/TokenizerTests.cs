﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Opaque;
using Assert = NUnit.Framework.Assert;
namespace Opaque.tests {
    [TestClass]
    public class TokenizerTests {
        [TestMethod]
        public void HappyFlowTokenize() {
            var t = Tokenizer.Tokenize("Command -flag -flag2 arg1 arg2");
            Assert.AreEqual(t.command, "Command");

            Assert.That(t.arguments.Contains("arg1"));
            Assert.That(t.arguments.Contains("arg2"));

            Assert.That(t.flags.Contains("flag"));
            Assert.That(t.flags.Contains("flag2"));
        }
        [TestMethod]
        public void HappyFlowTokenizeWithTarget() {
            var t = Tokenizer.Tokenize("$Target.Command -flag arg");

            Assert.AreEqual(t.target, "Target");
            Assert.AreEqual(t.command, "Command");

            Assert.That(t.flags.Contains("flag"));

            Assert.That(t.arguments.Contains("arg"));
        }
    }
}
