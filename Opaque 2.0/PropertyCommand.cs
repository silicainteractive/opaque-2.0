using System;
using System.ComponentModel;
using System.Reflection;

namespace Opaque
{
    public class PropertyCommand : Command {
        private readonly PropertyInfo property;
        public PropertyCommand(MemberInfo info): base(info) {
            property = info as PropertyInfo;
            if (property == null)
                throw new ArgumentException("Passed parameter info is not of type PropertyInfo");
           IsStatic = property.GetMethod.IsStatic;
        }
        protected override object Invoke(CommandEventArgs e) {
            if (e.args == null) return property.GetGetMethod().Invoke(e.target, null);
            var setValue = TypeDescriptor.GetConverter(property.PropertyType).ConvertFromInvariantString(e.args[0].ToString());
            var parameters = property.GetSetMethod().GetParameters().Length > 0 ? new[] { setValue } : null;
            return property.GetSetMethod().Invoke(e.target, parameters);
        }
    }
}