using System;
using System.Reflection;

namespace Opaque {
    public class MethodCommand : Command {
        private readonly MethodInfo method;
        public MethodCommand(MemberInfo info) : base(info) {
            method = info as MethodInfo;
            if (method == null)
                throw new ArgumentException("Passed parameter info is not of type MethodInfo");
            IsStatic = method.IsStatic;
        }
        protected override object Invoke(CommandEventArgs e) {
            return method.Invoke(e.target, e.args);
        }

    }
}