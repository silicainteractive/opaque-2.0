namespace Opaque
{
    public class CommandEventArgs {
        public readonly object target;
        public readonly object[] args;
        public readonly string[] flags;

        private CommandEventArgs(object target, object[] args, string[] flags = null) {
            this.target = target;
            this.args = args;
            this.flags = flags;
        }
        public static CommandEventArgs Create(object target, object[] args, string[] flags) {
            return new CommandEventArgs(target, args, flags);
        }
        public static CommandEventArgs Create(object target, params object[] args) {
            return new CommandEventArgs(target, args);
        }
    }
}