using System;
using System.Linq;
using static System.String;

namespace Opaque {
    public struct Tokens {
        public readonly string target;
        public readonly string command;
        public readonly object[] arguments;
        public readonly string[] flags;

        public Tokens(string target, string command, string[] arguments, string[] flags) {
            this.target = target;
            this.command = command;
            this.arguments = arguments;
            this.flags = flags;
        }
    }
    public static class Tokenizer {

        public static Tokens Tokenize(string input) {
            if (IsNullOrWhiteSpace(input))
                throw new ArgumentException("String should contain data");
            var split = input.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 1)
                throw new ArgumentException("String does not contain data");

            string target = null, command = null;
            string[] arguments = null, flags = null;


            target = split[0];

            if (target.StartsWith("$")) {
                if (target.Contains(".")) {
                    var tempSplit = target.Split(new[] { '.' }, 2, StringSplitOptions.RemoveEmptyEntries);
                    command = tempSplit[1];
                    target = tempSplit[0].Remove(0, 1);
                }
            } else {
                command = target;
            }

            if (split.Length < 2) return new Tokens(target, command, null, null);

            var argsplit = split[1].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            flags = argsplit.Where(s => s[0] == '-').Select(s => s.Remove(0, 1)).ToArray();
            arguments = argsplit.Except(flags).ToArray();

            return new Tokens(target, command, arguments, flags);
        }
    }
}