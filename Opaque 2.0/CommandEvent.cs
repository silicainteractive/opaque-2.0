namespace Opaque
{
    public delegate object CommandEvent(CommandEventArgs e);
}