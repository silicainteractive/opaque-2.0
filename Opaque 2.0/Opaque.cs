﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Opaque {

    public class Opaque {
        public readonly Dictionary<string, Command> registeredCommands;
        public readonly Dictionary<string, object> registeredInstances;

        public Dictionary<string, Command> StaticCommands {
            get {
                return registeredCommands.Where(kv => kv.Value.IsStatic).ToDictionary(k => k.Key, v => v.Value);
            }
        }
        public Dictionary<string, Command> InstanceCommands {
            get {
                return registeredCommands.Where(kv => !kv.Value.IsStatic).ToDictionary(k => k.Key, v => v.Value);
            }
        }
        private static Opaque _instance;
        public static Opaque Instance => _instance ?? (_instance = new Opaque());

        public static void Reset() {
            _instance = null;

        }
        private Opaque() {
            registeredCommands = new Dictionary<string, Command>();
            registeredInstances = new Dictionary<string, object>();
        }

        public void RegisterInstance(string name, object i) {
            registeredInstances.Add(name, i);
        }

        public void SetCommands() {
            var caller = Assembly.GetCallingAssembly();
            var members = caller.GetTypes()
                .SelectMany(t => t.GetMembers())
                .Where(m => m.GetCustomAttributes(typeof(CommandAttribute), false).Length > 0);
            foreach (var command in members.Select(Command.CreateCommand)) {
                registeredCommands.Add(command.Name, command);
            }
        }
        public object CallCommand(string command, CommandEventArgs e) {
            return registeredCommands.ContainsKey(command) ? registeredCommands[command].Execute(e) : null;
        }

        public object CallCommandFromTokens(Tokens t) {
            if (!registeredCommands.ContainsKey(t.command)) return null;
            object target = null;

            if (t.target != null && registeredInstances.ContainsKey(t.target))
                target = registeredInstances[t.target];

            var args = CommandEventArgs.Create(target, t.arguments, t.flags);

            return registeredCommands[t.command].Execute(args);
        }
    }
}
