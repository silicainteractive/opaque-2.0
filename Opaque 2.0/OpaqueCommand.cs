namespace Opaque
{
    public class OpaqueCommand : Command
    {
        private readonly CommandEvent command;
        public OpaqueCommand(string name, string helpText, CommandEvent e) : base(name, helpText)
        {
            command = e;
        }

        protected override object Invoke(CommandEventArgs e)
        {
            return command.Invoke(e);
        }
    }
}