﻿using System;
using System.Reflection;

namespace Opaque {
    public abstract class Command {
        public string Name { get; protected set; }
        public string HelpText { get; protected set; }
        protected event CommandEvent Functions;
        public bool IsStatic { get; protected set; }
        protected Command(MemberInfo info) {
            Functions += Invoke;
            var attr = info.GetCustomAttribute<CommandAttribute>();
            Name = attr.Name;
            HelpText = attr.HelpText;
        }

        protected Command(string name, string helpText) {
            Functions += Invoke;
            Name = name;
            HelpText = helpText;
        }
        public object Execute(CommandEventArgs e) {
            return Functions?.Invoke(e);
        }
        protected abstract object Invoke(CommandEventArgs e);

        public static Command CreateCommand(MemberInfo info) {
            Command retval;
            switch (info.MemberType) {
                case MemberTypes.Method:
                    retval = new MethodCommand(info);
                    break;
                case MemberTypes.Property:
                    retval = new PropertyCommand(info);
                    break;
                default:
                    throw new ArgumentException("MemberType {info.MemberType} is not supported yet.");
            }

            return retval;
        }

        public static Command CreateCommand(string name, string helpText, CommandEvent e) {
            
            return new OpaqueCommand(name, helpText, e);
        }
    }
}
