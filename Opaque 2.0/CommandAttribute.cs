﻿using System;

namespace Opaque {
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    public sealed class CommandAttribute : Attribute {
        public string HelpText { get; }
        public string Name { get; }
        public CommandAttribute(string name, string helpText)
        {
            Name = name;
            HelpText = HelpText;
        }
    }
}