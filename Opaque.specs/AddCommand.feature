﻿Feature: AddCommand
	In order to edit code on run-time
	As a user
	I want to expose methods and properties to Opaque

Background: Setup TestClass
	Given A clean instance of Opaque
	And I have an instance of type TestClass

Scenario: Registering Commands
	Given I have a class method annotated with a CommandAttribute
	When I call Opaque's SetCommands method
	Then The method should be registered in Opaque

Scenario: Calling Methods
	Given A registered method within Opaque
	When I call a method through Opaque
	Then The registered method should be called

Scenario Outline: Changing Properties
	Given a registered float property within Opaque
	When I set the property to <float>
	Then the get method should return <float>

	Examples:
	| float |
	| 5     |
	| 15    |
	| 48    |
	| 137   |

