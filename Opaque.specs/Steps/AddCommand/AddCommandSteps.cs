﻿using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Opaque;
using TechTalk.SpecFlow;

namespace OpaqueNS.specs.Steps.AddCommand {
    [Binding]
    public class AddCommandSteps {
        class TestClass {
            public int calledCount;
            [Command("TestMethod", "none")]
            public void TestMethod() {
                calledCount++;
            }
            [Command("TestProperty", "none")]
            // ReSharper disable once UnusedMember.Local
            public float TestProperty { get; set; }
            // ReSharper disable once MemberCanBeMadeStatic.Local
            public void RegisterToOpaque() {
                Opaque.Opaque.Instance.SetCommands();
            }
        }

        #region background
        [Given(@"A clean instance of Opaque")]
        public void GivenACleanInstanceOfOpaque() {
            Opaque.Opaque.Reset();
        }
        private TestClass tester;
        [Given(@"I have an instance of type TestClass")]
        public void GivenAnInstanceOfTypeTestClass() {
            tester = new TestClass();

        }
        #endregion

        #region registering
        private MethodInfo methodinfo;
        [Given(@"I have a class method annotated with a CommandAttribute")]
        public void GivenIHaveAClassMethodAnnotatedWithACommandAttribute() {

            methodinfo = tester.GetType().GetMethods().FirstOrDefault(m => m.GetCustomAttribute<CommandAttribute>() != null);
            Assert.AreNotEqual(methodinfo, null);
        }

        [When(@"I call Opaque's SetCommands method")]
        public void WhenICallOpaquesSetCommandsMethod() {
            tester.RegisterToOpaque();
        }

        [Then(@"The method should be registered in Opaque")]
        public void ThenTheMethodShouldBeRegisteredInOpaque() {
            Assert.Contains(methodinfo.Name, Opaque.Opaque.Instance.registeredCommands.Keys);
        }
        #endregion

        #region calling
        [Given(@"A registered method within Opaque")]
        public void GivenARegisteredMethodWithInOpaque() {
            tester.RegisterToOpaque();
        }

        [When(@"I call a method through Opaque")]
        public void WhenICallAMethodThroughOpaque() {
            Opaque.Opaque.Instance.CallCommand("TestMethod", CommandEventArgs.Create(tester, null));
        }

        [Then(@"The registered method should be called")]
        public void ThenTheRegisteredMethodShouldBeCalled() {
            Assert.AreEqual(1, tester.calledCount);
        }
        #endregion
        [Given(@"a registered float property within Opaque")]
        public void GivenARegisteredFloatPropertyWithinOpaque() {
            tester.RegisterToOpaque();
        }
        #region properties
        [When(@"I set the property to (.*)")]
        public void WhenISetThePropertyTo(float p0) {
            Opaque.Opaque.Instance.CallCommand("TestProperty", CommandEventArgs.Create(tester, p0));
        }

        [Then(@"the get method should return (.*)")]
        public void ThenTheGetMethodShouldReturn(float p0) {
            var check = (float)Opaque.Opaque.Instance.CallCommand("TestProperty", CommandEventArgs.Create(tester, null));
            Assert.That(p0, Is.EqualTo(check));
        }
        #endregion
    }
}
